use std::time;

const TEN_MIN: time::Duration = time::Duration::from_secs(600);
const MAX_SIZE: usize = 128;

struct Element<T> {
    timestamp: time::Instant,
    value: T
}

impl<T> Element<T> {
    fn new(value: T) -> Self {
        Self {
            timestamp: time::Instant::now(),
            value
        }
    }
}

pub struct Drain<'a, T: 'a> {
    queue: &'a mut RingQueue<T>,
    time_limit: time::Instant,
}

impl<'a, T> Iterator for Drain<'a, T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = None;
        loop {
            if self.queue.start == self.queue.end {
                self.queue.start = 0;
                self.queue.end = 0;
                break;
            } else {
                let item = self.queue.pop_unchecked();

                if item.timestamp >= self.time_limit {
                    result = Some(item.value);
                    break;
                }
            }
        }

        result
    }
}

/**
 * Simple Circular Buffer that stores element insertion timestamps.
 *
 * On overflow it starts inserting elements instead of old ones.
 * To retrieve all elements, use `drain()` method which produces Iterator
 * of all elements not older than `TEN_MIN`
 */
pub struct RingQueue<T> {
    inner: Vec<Option<Element<T>>>,
    start: usize,
    end: usize,
}

impl<T> RingQueue<T> {
    pub fn new() -> Self {
        let mut queue = RingQueue {
            inner: Vec::with_capacity(MAX_SIZE),
            start: 0,
            end: 0
        };

        for _ in 0..512 {
            queue.inner.push(None);
        }

        queue
    }

    pub fn push(&mut self, value: T) {
        self.inner[self.end] = Some(Element::new(value));
        self.end = (self.end + 1) % MAX_SIZE;

        if self.start == self.end {
            for _ in 0..10 {
                self.inner[self.start] = None;
                self.start = (self.start + 1) % MAX_SIZE;
            }
        }
    }

    fn pop_unchecked(&mut self) -> Element<T> {
        let start = self.start;

        let item = self.inner[start].take().expect("To have Element");
        self.start = (start + 1) % MAX_SIZE;
        item
    }

    pub fn drain<'a>(&'a mut self) -> Drain<'a, T> {
        Drain {
            queue: self,
            time_limit: time::Instant::now() - TEN_MIN
        }
    }

    #[allow(dead_code)]
    pub fn reset(&mut self) {
        self.start = 0;
        self.end = 0;
        for idx in 0..MAX_SIZE {
            self.inner[idx] = None;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_queue() {
        let mut queue = RingQueue::<usize>::new();

        for idx in 0..MAX_SIZE+9 {
            queue.push(idx);
        }

        let drained = queue.drain().collect::<Vec<_>>();
        assert_eq!(drained.len(), MAX_SIZE - 1);

        for (idx, expected_item) in (10..MAX_SIZE+9).enumerate() {
            assert_eq!(drained[idx], expected_item);
        }

        for idx in 0..MAX_SIZE+9 {
            queue.push(idx);
        }

        let drained = queue.drain().collect::<Vec<_>>();
        assert_eq!(drained.len(), MAX_SIZE - 1);

        for (idx, expected_item) in (10..MAX_SIZE+9).enumerate() {
            assert_eq!(drained[idx], expected_item);
        }
    }
}
