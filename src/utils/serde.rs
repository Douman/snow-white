use serde::{de, ser};
use chrono;

use std::borrow::Cow;

pub mod naive_time {
    use super::*;

    pub const FORMAT: &'static str = "%H:%M:%S";
    pub fn serialize<S: ser::Serializer>(time: &chrono::NaiveTime, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_str(&time.format(FORMAT).to_string())
    }

    pub fn parse_from_str(time_str: &str) -> Result<chrono::NaiveTime, chrono::ParseError> {
        chrono::NaiveTime::parse_from_str(time_str, FORMAT)
    }

    pub fn deserialize<'de, D: de::Deserializer<'de>>(deserializer: D) -> Result<chrono::NaiveTime, D::Error> {
        use serde::{Deserialize};
        let time_str: Cow<'de, str> = Cow::deserialize(deserializer)?;

        parse_from_str(&time_str).map_err(de::Error::custom)
    }
}
