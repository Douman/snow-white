use std::env;
use std::path::PathBuf;

///Retrieves path to configuration.
///
///Requires name of config.
pub fn get_app_config(name: &str) -> PathBuf {
    let mut result = env::current_exe().unwrap();

    result.set_file_name(name);

    result
}
