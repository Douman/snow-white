#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_panic;
#[macro_use]
extern crate lazy_static;
extern crate serde;
#[macro_use]
extern crate serde_derive;

mod constants;
mod rt;
#[macro_use]
mod utils;
mod talk;
mod random;
mod system;

fn main() {
    rt::ssl::init();
    rt::log::init();

    system::start();
}
