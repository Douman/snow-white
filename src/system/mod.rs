use actix::{self, Actor};

mod stats;
mod message;
mod discord;

use self::discord::Discord;

pub fn start() {
    info!("Start Snow White");
    let system = actix::System::new("Snow White".to_owned());

    let _discord = Discord::default().start();

    let result = system.run();
    if result != 0 {
        warn!("System finished with result={}", result);
    }
}
