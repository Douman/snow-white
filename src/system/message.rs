//! Common messages for system

use actix;
use serenity;

use std::fmt;

use super::stats::STATS;

pub struct StopActor;
impl actix::Message for StopActor {
    type Result = ();
}

pub enum PlainMessage {
    Static(&'static str),
    Dynamic(String),
}

impl fmt::Display for PlainMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &PlainMessage::Static(text) => write!(f, "{}", text),
            &PlainMessage::Dynamic(ref text) => write!(f, "{}", text),
        }
    }
}

impl From<String> for PlainMessage {
    fn from(msg: String) -> Self {
        PlainMessage::Dynamic(msg)
    }
}

impl From<&'static str> for PlainMessage {
    fn from(msg: &'static str) -> Self {
        PlainMessage::Static(msg)
    }
}

impl From<PlainMessage> for MessageType {
    fn from(msg: PlainMessage) -> Self {
        MessageType::Plain(msg)
    }
}

pub struct EmbedMessage {
    pub title: &'static str,
    pub description: PlainMessage,
    pub image: Option<&'static str>,
}

impl EmbedMessage {
    pub fn new<M: Into<PlainMessage>>(title: &'static str, description: M) -> Self {
        Self {
            title,
            description: description.into(),
            image: None
        }
    }

    fn build(&self, builder: serenity::builder::CreateEmbed) -> serenity::builder::CreateEmbed {
        let builder = builder.title(self.title).description(&self.description).color(serenity::utils::Colour::DARK_RED);
        match self.image {
            Some(image) => builder.image(image),
            None => builder
        }
    }
}

impl From<EmbedMessage> for MessageType {
    fn from(msg: EmbedMessage) -> Self {
        MessageType::Embed(msg)
    }
}

pub enum MessageType {
    Plain(PlainMessage),
    Embed(EmbedMessage),
    EmbedWithMsg(EmbedMessage, PlainMessage),
}

pub struct SendMessage {
    channel: u64,
    msg_type: MessageType
}

impl SendMessage {
    pub fn new<M: Into<MessageType>>(channel: u64, msg_type: M) -> Self {
        Self {
            channel,
            msg_type: msg_type.into(),
        }
    }

    pub fn plain<M: Into<PlainMessage>>(channel: u64, content: M) -> Self {
        Self::new(channel, content.into())
    }

    pub fn embed(channel: u64, embed: EmbedMessage) -> Self {
        Self::new(channel, embed)
    }

    pub fn embed_with_msg<M: Into<PlainMessage>>(channel: u64, msg: M, embed: EmbedMessage) -> Self {
        Self::new(channel, MessageType::EmbedWithMsg(embed, msg.into()))
    }

    pub fn send(&self) -> serenity::Result<serenity::model::channel::Message> {
        let channel = serenity::model::id::ChannelId(self.channel);

        match &self.msg_type {
            MessageType::Plain(text) => channel.say(text),
            MessageType::Embed(embed) => channel.send_message(|msg| {
                msg.embed(|msg| embed.build(msg))
            }),
            MessageType::EmbedWithMsg(embed, content) => channel.send_message(|msg| {
                msg.embed(|msg| embed.build(msg))
                   .content(content)
            }),
        }
    }
}

impl actix::Message for SendMessage {
    type Result = ();
}

pub struct SendStats(pub u64);

impl SendStats {
    pub fn send(&self) -> serenity::Result<serenity::model::channel::Message> {
        let channel = serenity::model::id::ChannelId(self.0);

        channel.send_message(|msg| {
            msg.embed(|embed| embed.title("Stats").color(serenity::utils::Colour::DARK_RED)
                                   .field("Discord", &STATS.discord, true)
                                   .field("Guild", &STATS.guild, true)
                                   .field("Actor", &STATS.actor, true))
        })
    }
}

impl actix::Message for SendStats {
    type Result = ();
}
