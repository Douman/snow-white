use chrono;
use serenity;
use futures;
use futures::sync::mpsc;
use actix::{self, ActorFuture, AsyncContext, WrapFuture, ActorContext};
use tokio;
use mmap_storage;
use cute_dnd_dice::Roll;
use serenity::prelude::Mentionable;

use std::thread;
use std::time;
use std::collections::HashSet;

use super::message;
use super::stats::{self, STATS};
use crate::utils::args;
use crate::constants::*;
use crate::talk;

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Channels {
    welcome: u64,
}

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    #[serde(with = "crate::utils::serde::naive_time")]
    colo_time: chrono::NaiveTime,
    nots: Notifications,
    #[serde(default)]
    channels: Channels,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            //22:00 JST
            colo_time: chrono::NaiveTime::from_hms(13, 0, 0),
            nots: Notifications::default(),
            channels: Channels::default(),
        }
    }
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Notifications {
    colo: HashSet<u64>,
    evo: HashSet<u64>,
}

#[derive(Debug)]
struct DiscordMessage(pub serenity::model::channel::Message);

impl From<serenity::model::channel::Message> for DiscordMessage {
    fn from(msg: serenity::model::channel::Message) -> DiscordMessage {
        DiscordMessage(msg)
    }
}
impl From<DiscordMessage> for serenity::model::channel::Message {
    fn from(wrapper: DiscordMessage) -> serenity::model::channel::Message {
        wrapper.0
    }
}
impl actix::Message for DiscordMessage {
    type Result = ();
}

#[derive(Debug)]
struct ColoNotification;
impl actix::Message for ColoNotification {
    type Result = ();
}

#[derive(Debug)]
struct ColoGridNotification;
impl actix::Message for ColoGridNotification {
    type Result = ();
}

#[derive(Debug)]
struct DarkNotification;
impl actix::Message for DarkNotification {
    type Result = ();
}

#[derive(Clone)]
struct MessageHandler {
    sender: mpsc::UnboundedSender<DiscordMessage>,
    welcome_sender: mpsc::UnboundedSender<WelcomeUser>,
}

impl serenity::prelude::EventHandler for MessageHandler {
    fn ready(&self, _ctx: serenity::prelude::Context, _bot_data: serenity::model::gateway::Ready) {
        STATS.increment(stats::DiscordConnected);
    }

    fn resume(&self, _ctx: serenity::prelude::Context, _: serenity::model::event::ResumedEvent) {
        STATS.increment(stats::DiscordReConnected);
    }

    fn message(&self, _ctx: serenity::prelude::Context, msg: serenity::model::channel::Message) {
        // We ignore bot messages
        if msg.author.bot == true {
            return;
        }
        else if msg.content.starts_with(CMD_PREFIX) {
            match self.sender.unbounded_send(msg.into()) {
                Ok(_) => (),
                Err(_) => {
                    STATS.increment(stats::DiscordBrokenPipe);
                }
            }
        } else if let Some(rsp) = talk::reply_to(&msg.content) {
            let _ = msg.reply(rsp);
        }
    }

    fn guild_member_addition(&self, _: serenity::prelude::Context, _: serenity::model::id::GuildId, user: serenity::model::guild::Member) {
        STATS.increment(stats::DiscordNewMember);

        let msg = WelcomeUser {
            user
        };
        match self.welcome_sender.unbounded_send(msg) {
            Ok(_) => (),
            Err(_) => {
                STATS.increment(stats::DiscordBrokenPipe);
            }
        }
    }
}

#[derive(Default)]
struct Timers {
    colo: Option<[actix::SpawnHandle; 2]>,
    config_update: Option<actix::SpawnHandle>,
    dark: Option<[actix::SpawnHandle; 4]>,
}

pub struct Discord {
    timers: Timers,
    config: Config,
    config_storage: mmap_storage::serializer::FileView<Config, mmap_storage::serializer::Toml>,
    shard_manager: Option<::std::sync::Arc<serenity::prelude::Mutex<serenity::client::bridge::gateway::ShardManager>>>,
    wait_buffer: crate::utils::RingQueue<message::SendMessage>,
    owner: Option<serenity::model::user::User>,
}

impl Default for Discord {
    fn default() -> Self {
        let config_path = crate::utils::path::get_app_config(CONFIG_NAME);
        let config_storage = mmap_storage::serializer::FileView::<Config, mmap_storage::serializer::Toml>::open_or_default(config_path).unwrap();
        let config = config_storage.load_owned().unwrap();

        Discord {
            timers: Timers::default(),
            config,
            config_storage,
            shard_manager: None,
            wait_buffer: crate::utils::RingQueue::new(),
            owner: None,
        }
    }
}

impl Discord {
    fn idle_msg(channel_id: u64, is_master: bool) -> message::SendMessage {
        const MASTER_LINES: [&'static str; 3] = [
            "Master, what is it? Do you need anything? Please tell me, I'll do anything for you.",
            "My loyalty belongs to you, my Master",
            "My Master, I beseech you: please, spin the story again",
        ];
        const OTHER_LINES: [&'static str; 6] = [
            "What do you want? If nothing, then please do not disturb me.",
            "Only fair end should await the righteous one",
            "I'm... not wrong...",
            "My justice is... the revival of my master. Those who would dare to stand in its way... are but the evil",
            "Even if I have to dye my hands in blood, I shall carry on my justice",
            "Am I really interesting...?"
        ];

        let text = match is_master {
            true => MASTER_LINES[crate::random::range(0, MASTER_LINES.len())],
            false => OTHER_LINES[crate::random::range(0, OTHER_LINES.len())],
        };
        message::SendMessage::plain(channel_id, text)
    }

    fn set_welcome(&mut self, msg: &serenity::model::channel::Message) {
        match msg.guild() {
            Some(_) => (),
            None => {
                let _ = msg.reply(GUILD_ONLY_CMD);
                return;
            }
        };

        self.config.channels.welcome = msg.channel_id.0;
        let _ = msg.reply(WELCOME_SET);
    }

    fn is_started(&self) -> bool {
        self.shard_manager.is_some()
    }

    ///Schedules to update config
    ///
    ///Update is delayed by UPDATE_CONFIG_PERIOD
    fn update_config(&mut self, ctx: &mut actix::Context<Self>) {
        if self.timers.config_update.is_none() {
            let handle = ctx.run_later(UPDATE_CONFIG_PERIOD, |act, _ctx| {
                match act.config_storage.save_async(&act.config) {
                    Ok(_) => debug!("Discord: Updated config"),
                    Err(_) => {
                        STATS.increment(stats::DiscordBrokenConfigUpdate);
                    }
                }
                act.timers.config_update = None;
            });

            self.timers.config_update = Some(handle);
        }
    }

    fn shutdown_discord_worker(&mut self) {
        if let Some(shard_manager) = self.shard_manager.take() {
            shard_manager.lock().shutdown_all();
        }
    }

    fn config_dump(&self) {
        if let Some(owner) = self.owner.as_ref() {
            match owner.create_dm_channel() {
                Ok(channel) => {
                    let config = match toml::to_vec(&self.config) {
                        Ok(config) => config,
                        Err(error) => {
                            warn!("Discord: Unable to serialize config. Error: {}", error);
                            return;
                        }
                    };

                    let config = config.as_slice();
                    let attach = vec![(config, CONFIG_NAME)];
                    match channel.send_files(attach, |msg| msg.content("Here is config, Master")) {
                        Ok(_) => (),
                        Err(error) => warn!("Discord: Unable to send config. Error: {}", error),
                    }
                },
                Err(_) => {
                    STATS.increment(stats::DiscordDmFail);
                }
            }
        }
    }

    fn dm_master(&mut self, message: &str) {
        if self.owner.is_none() {
            match serenity::http::raw::get_current_application_info() {
                Ok(app_info) => {
                    self.owner = Some(app_info.owner);
                },
                Err(_) => {
                    STATS.increment(stats::DiscordNoAppInfo);
                    return;
                }
            };
        }

        if let Some(owner) = self.owner.as_ref() {
            let _ = owner.direct_message(|msg| msg.content(message));
        }
    }

    fn send_help_message(ctx: &mut actix::Context<Self>, channel: u64, error: Option<message::PlainMessage>) {
        let embed = message::EmbedMessage::new(HELP_TITLE, HELP_TEXT);

        match error {
            Some(error) => ctx.notify(message::SendMessage::embed_with_msg(channel, error, embed)),
            None => ctx.notify(message::SendMessage::embed(channel, embed)),
        }
    }

    fn inner_send(message: &message::SendMessage) -> Result<(), ()> {
        match message.send() {
            Ok(_) => Ok(()),
            Err(serenity::Error::Http(serenity::http::HttpError::UnsuccessfulRequest(_))) => {
                //Most likely either bot is forbidden or it is request for non-existing channel.
                STATS.increment(stats::DiscordMsgReject);
                Ok(())
            },
            //TODO: There may be other errors that do not require to restart.
            Err(error) => {
                error!("Discord: Unable to send message. Error: {:?}", error);
                STATS.increment(stats::DiscordMsgFail);
                Err(())
            }
        }
    }

    fn send_message(&mut self, ctx: &mut actix::Context<Self>, msg: message::SendMessage) {
        use actix::ActorContext;

        if !self.is_started() {
            return self.wait_buffer.push(msg);
        }

        match Self::inner_send(&msg) {
            Ok(_) => (),
            Err(_) => {
                self.wait_buffer.push(msg);
                ctx.stop();
            }
        }
    }

    fn send_current_time(ctx: &mut actix::Context<Self>, channel_id: u64) {
        let now = chrono::Utc::now();
        let utc_time = now.time();
        let jst_time = now.with_timezone(&chrono::offset::FixedOffset::east(JST_OFFSET)).time();

        //Approximate number of characters to be written
        let text = format!("__UTC__\n**{}**\n__JST__\n**{}**\n", utc_time.format(crate::utils::serde::naive_time::FORMAT), jst_time.format(crate::utils::serde::naive_time::FORMAT));
        ctx.notify(message::SendMessage::embed(channel_id, message::EmbedMessage::new("Time", text)))
    }

    fn set_new_colo_time(&mut self, ctx: &mut actix::Context<Self>, time: chrono::NaiveTime) {
        use chrono::Timelike;

        info!("Discord: New colo time {} is set", time);
        STATS.increment(stats::GuildNewColo);

        let time = chrono::Utc::now().with_timezone(&chrono::offset::FixedOffset::east(JST_OFFSET))
                                     .with_hour(time.hour()).unwrap()
                                     .with_minute(time.minute()).unwrap()
                                     .with_second(time.second()).unwrap()
                                     .with_timezone(&chrono::offset::Utc)
                                     .time();

        self.config.colo_time = time;
        self.update_config(ctx);

        if let Some(colo) = self.timers.colo.take() {
            for colo in colo.into_iter() {
                ctx.cancel_future(*colo);
            }
        }
        self.setup_timers(ctx);
    }

    fn setup_timers(&mut self, ctx: &mut actix::Context<Self>) {
        use chrono::Timelike;
        use futures::Stream;

        let now = chrono::Utc::now();
        let now_time = now.time();

        info!("Discord: Time now {}", now);

        if self.timers.colo.is_none() {
            STATS.increment(stats::GuildSetColoTimer);
            let next_colo = match now_time > self.config.colo_time {
                true => {
                    info!("Discord: Colo time passed");
                    let next = now + chrono::Duration::days(1);
                    next.with_hour(self.config.colo_time.hour() - 1).unwrap()
                        .with_minute(45).unwrap()
                },
                false => {
                    info!("Discord: Colo time is coming on this day.");
                    now.with_hour(self.config.colo_time.hour() - 1).unwrap()
                       .with_minute(45).unwrap()
                }
            };

            let colo_interval = next_colo - now;
            let colo_interval = colo_interval.to_std().unwrap();

            let mut next_grid = next_colo.with_minute(15).unwrap();
            if next_grid < now {
                next_grid = next_grid + chrono::Duration::days(1);
            }
            let grid_interval = next_grid - now;
            let grid_interval = grid_interval.to_std().unwrap();

            info!("Discord: Next colo notification {}", next_colo);
            info!("Discord: Next grid notification {}", next_grid);

            let colo_interval = tokio::timer::Interval::new(time::Instant::now() + colo_interval, DAY_DURATION);
            let grid_interval = tokio::timer::Interval::new(time::Instant::now() + grid_interval, DAY_DURATION);
            let colo_interval = ctx.add_stream(colo_interval.map(|_| ColoNotification).map_err(|_| ()));
            let grid_interval = ctx.add_stream(grid_interval.map(|_| ColoGridNotification).map_err(|_| ()));
            self.timers.colo = Some([colo_interval, grid_interval]);
        }

        if self.timers.dark.is_none() {
            STATS.increment(stats::GuildSetDarkTimer);
            //First evo quest is at 7:30 JST
            let event_time = chrono::NaiveTime::from_hms(22, 30, 0); //UTC time of previous day
            let first = match now_time > event_time {
                true => {
                    info!("Discord: First evo event time passed");
                    let next = now + chrono::Duration::days(1);
                    next.with_hour(event_time.hour()).unwrap()
                        .with_minute(event_time.minute() - 15).unwrap()
                },
                false => {
                    info!("Discord: First evo event time is coming");
                    now.with_hour(event_time.hour()).unwrap()
                       .with_minute(event_time.minute() - 15).unwrap()
                },
            };

            info!("Discord: First evo event notification {}", first);
            let duration = first - now;
            let duration = duration.to_std().unwrap();

            let interval = tokio::timer::Interval::new(time::Instant::now() + duration, DAY_DURATION);
            let first = ctx.add_stream(interval.map(|_| DarkNotification).map_err(|_| ()));

            //Second evo quest is at 12:00 JST
            let event_time = chrono::NaiveTime::from_hms(3, 0, 0);
            let second = match now_time > event_time {
                true => {
                    info!("Discord: Second evo event time passed");
                    let next = now + chrono::Duration::days(1);
                    next.with_hour(event_time.hour() - 1).unwrap()
                        .with_minute(45).unwrap()
                },
                false => {
                    info!("Discord: Second evo event time is coming");
                    now.with_hour(event_time.hour() - 1).unwrap()
                       .with_minute(45).unwrap()
                },
            };

            info!("Discord: Second evo event notification {}", second);
            let duration = second - now;
            let duration = duration.to_std().unwrap();

            let interval = tokio::timer::Interval::new(time::Instant::now() + duration, DAY_DURATION);
            let second = ctx.add_stream(interval.map(|_| DarkNotification).map_err(|_| ()));

            //Third evo quest is at 19:30 JST
            let event_time = chrono::NaiveTime::from_hms(10, 30, 0);
            let third = match now_time > event_time {
                true => {
                    info!("Discord: Third evo event time passed");
                    let next = now + chrono::Duration::days(1);
                    next.with_hour(event_time.hour()).unwrap()
                        .with_minute(event_time.minute() - 15).unwrap()
                },
                false => {
                    info!("Discord: Third evo event time is coming");
                    now.with_hour(event_time.hour()).unwrap()
                       .with_minute(event_time.minute() - 15).unwrap()
                },
            };

            info!("Discord: Third evo event notification {}", third);
            let duration = third - now;
            let duration = duration.to_std().unwrap();

            let interval = tokio::timer::Interval::new(time::Instant::now() + duration, DAY_DURATION);
            let third = ctx.add_stream(interval.map(|_| DarkNotification).map_err(|_| ()));

            //Fourth evo quest is at 22:30 JST
            let event_time = chrono::NaiveTime::from_hms(13, 30, 0);
            let fourth = match now_time > event_time {
                true => {
                    info!("Discord: Fourth evo event time passed");
                    let next = now + chrono::Duration::days(1);
                    next.with_hour(event_time.hour()).unwrap()
                        .with_minute(event_time.minute() - 15).unwrap()
                },
                false => {
                    info!("Discord: Fourth evo event time is coming");
                    now.with_hour(event_time.hour()).unwrap()
                       .with_minute(event_time.minute() - 15).unwrap()
                },
            };

            info!("Discord: Fourth evo event notification {}", fourth);
            let duration = fourth - now;
            let duration = duration.to_std().unwrap();

            let interval = tokio::timer::Interval::new(time::Instant::now() + duration, DAY_DURATION);
            let fourth = ctx.add_stream(interval.map(|_| DarkNotification).map_err(|_| ()));

            self.timers.dark = Some([first, second, third, fourth]);
        }
    }
}

impl actix::Actor for Discord {
    type Context = actix::Context<Self>;

    fn stopping(&mut self, _ctx: &mut Self::Context) -> actix::Running {
        info!("Discord: Stopping");
        STATS.increment(stats::ActorStop);

        self.shutdown_discord_worker();

        actix::Running::Stop
    }

    fn started(&mut self, ctx: &mut actix::Context<Self>) {
        info!("Discord: Starting");
        STATS.increment(stats::ActorStart);

        self.setup_timers(ctx);

        let (welcome_sender, welcome_receiver) = futures::sync::mpsc::unbounded();
        let (sender, receiver) = futures::sync::mpsc::unbounded();
        let handler = MessageHandler { sender, welcome_sender };

        let (manager_sender, manager_recv) = futures::sync::oneshot::channel();

        thread::Builder::new().name("discord-worker".to_owned()).spawn(move || {
            let mut client = serenity::client::Client::new(TOKEN, handler.clone()).expect("Error creating client");
            manager_sender.send(client.shard_manager.clone()).expect("To send shard_manager");

            match client.start() {
                Ok(_) => {
                    STATS.increment(stats::DiscordShutdown);
                },
                Err(_why) => {
                    warn!("Discord: Finished with error: {}", _why);
                    STATS.increment(stats::DiscordFailure);
                }
            }
        }).expect("To create discord-worker thread");

        ctx.add_stream(receiver);
        ctx.add_stream(welcome_receiver);
        let manager_recv = manager_recv.into_actor(self).map(|manager, act, _ctx| {
            act.shard_manager = Some(manager);
            act.dm_master("Master, Snow is here");

            for msg in act.wait_buffer.drain() {
                let _ = Self::inner_send(&msg);
            }
        }).map_err(|_, _act, ctx| {
            warn!("Discord: Couldn't get shard manager. Cancelled future");
            ctx.stop();
        });

        ctx.spawn(manager_recv);
    }
}

impl actix::StreamHandler<DiscordMessage, ()> for Discord {
    fn error(&mut self, _error: (), _ctx: &mut Self::Context) -> actix::Running {
        warn!("Discord: Error handling messages");
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        use actix::ActorContext;

        warn!("Discord: Message stream is closed");
        ctx.stop();
    }

    fn handle(&mut self, msg: DiscordMessage, ctx: &mut Self::Context) {
        let msg = msg.0;
        debug!("Discord: {:?}", &msg);

        let content = msg.content.trim();

        //We check it in serenity handler
        //if !content.starts_with(CMD_PREFIX) {
        //    return;
        //}

        let content = &content[CMD_PREFIX.len()..];

        let args = match args::shell_split(content) {
            Ok(args) => args,
            Err(_) => return,
        };

        let counter = STATS.increment(stats::DiscordCmdNum);

        match args.len() {
            0 => ctx.notify(Self::idle_msg(msg.channel_id.0, msg.author.id.0 == self.owner.as_ref().map(|owner| owner.id.0).unwrap_or(0))),
            1 => match args[0] {
                "help" => Self::send_help_message(ctx, msg.channel_id.0, None),
                "restart" => match msg.author.id.0 == self.owner.as_ref().map(|owner| owner.id.0).unwrap_or(0) {
                    true => ctx.notify(message::StopActor),
                    false => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "You have no authority to issue such order, only my Master can. Begone!"))
                },
                "config_dump" => match msg.author.id.0 == self.owner.as_ref().map(|owner| owner.id.0).unwrap_or(0) {
                    true => self.config_dump(),
                    false => counter.forget(),
                },
                "library" => ctx.notify(message::SendMessage::embed(msg.channel_id.0, message::EmbedMessage::new(LIBRARY_TITLE, LIBRARY_TEXT))),
                "judge" => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Go ahead, give me someone to judge")),
                "roll" => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Tell me what to roll")),
                "time" => Self::send_current_time(ctx, msg.channel_id.0),
                "subscribe" => ctx.notify(message::SendMessage::embed(msg.channel_id.0, message::EmbedMessage::new("Subscribe", HELP_SUBSCRIBE))),
                "unsubscribe" => ctx.notify(message::SendMessage::embed(msg.channel_id.0, message::EmbedMessage::new("Unsubscribe", HELP_UNSUB))),
                "set_colo_time" => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Please, tell me new time for Colo")),
                "set_welcome" => {
                    self.set_welcome(&msg);
                    self.update_config(ctx);
                },
                "stats" => {
                    match msg.author.id.0 == self.owner.as_ref().map(|owner| owner.id.0).unwrap_or(0) {
                        true => ctx.notify(message::SendStats(msg.channel_id.0)),
                        false => (),
                    }
                    counter.forget();
                },
                _ => counter.forget(),
            }
            2 => match args[0] {
                "help" => {
                    let embed = match args[1] {
                        "subscribe" => message::EmbedMessage::new("Subscribe", HELP_SUBSCRIBE),
                        "unsubscribe" => message::EmbedMessage::new("Unsubscribe", HELP_UNSUB),
                        "library" => message::EmbedMessage::new("Library", HELP_LIBRARY),
                        "judge" => message::EmbedMessage::new("Judge", HELP_JUDGE),
                        "time" => message::EmbedMessage::new("Time", HELP_TIME),
                        "set_colo_time" => message::EmbedMessage::new("Set Colo Time", HELP_SET_COLO_TIME),
                        "roll" => message::EmbedMessage::new("Roll dice", HELP_ROLL),
                        "set_welcome" => message::EmbedMessage::new("Roll dice", HELP_SET_WELCOME),
                        _ => return Self::send_help_message(ctx, msg.channel_id.0, Some("Unknown command".into())),
                    };

                    ctx.notify(message::SendMessage::embed(msg.channel_id.0, embed));
                },
                "roll" => match Roll::from_str(&content[4..]) {
                    Ok(roll) => ctx.notify(message::SendMessage::plain(msg.channel_id.0, format!("You roll {}", roll.roll()))),
                    Err(error) => ctx.notify(message::SendMessage::plain(msg.channel_id.0, format!("Cannot parse your roll: {}. Try better", error))),
                },
                "judge" => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "I need more than one to judge!")),
                "subscribe" => match args[1] {
                    "colo" => match self.config.nots.colo.insert(msg.channel_id.0) {
                        true => {
                            ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Channel has been subscribed"));
                            self.update_config(ctx);
                        },
                        false => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Already subscribed")),
                    },
                    "evo" => match self.config.nots.evo.insert(msg.channel_id.0) {
                        true => {
                            ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Channel has been subscribed"));
                            self.update_config(ctx);
                        },
                        false => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Already subscribed")),
                    },
                    _ => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Unknown type of notification")),
                },
                "unsubscribe" => match args[1] {
                    "colo" => match self.config.nots.colo.remove(&msg.channel_id.0) {
                        true => {
                            ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Subscription has been removed"));
                            self.update_config(ctx);
                        },
                        false => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "There is no subscription already")),
                    },
                    "evo" => match self.config.nots.evo.remove(&msg.channel_id.0) {
                        true => {
                            ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Subscription has been removed"));
                            self.update_config(ctx);
                        },
                        false => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "There is no subscription already")),
                    },
                    _ => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Unknown type of notification")),
                },
                "set_colo_time" => match crate::utils::serde::naive_time::parse_from_str(&args[1]) {
                    Ok(new_time) => {
                        self.set_new_colo_time(ctx, new_time);
                        ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Successfully set new colo time, resetting notification..."));
                    },
                    Err(error) => {
                        debug!("Discord: Failed to parse time '{}'. Error: {}", args[1], error);
                        ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Invalid time"));
                    }
                },
                _ => counter.forget(),
            },
            _ => match args[0] {
                "judge" => {
                    let idx = crate::random::range(1, args.len());

                    let name = args[idx].trim_matches(',');
                    let text = if name.starts_with("<@") {
                        let opts = serenity::utils::ContentSafeOptions::new().clean_user(false).clean_channel(true).show_discriminator(false);
                        let name = serenity::utils::content_safe(name, &opts);
                        format!("Be judged {}", name)
                    } else {
                        format!("Bu judged `{}`", name)
                    };
                    ctx.notify(message::SendMessage::plain(msg.channel_id.0, text));
                },
                "roll" => match Roll::from_str(&content[4..]) {
                    Ok(roll) => ctx.notify(message::SendMessage::plain(msg.channel_id.0, format!("You roll {}", roll.roll()))),
                    Err(error) => ctx.notify(message::SendMessage::plain(msg.channel_id.0, format!("Cannot parse your roll: {}. Try better", error))),
                },
                "help" | "restart" | "subscribe" | "unsubscribe" | "library" | "set_colo_time" | "set_welcome" => ctx.notify(message::SendMessage::plain(msg.channel_id.0, "Invalid number of arguments")),
                _ => counter.forget(),
            },
        }
    }
}

impl actix::StreamHandler<ColoNotification, ()> for Discord {
    fn error(&mut self, _error: (), _ctx: &mut Self::Context) -> actix::Running {
        error!("Discord: Error handling Colo Notification");
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        use actix::ActorContext;

        error!("Discord: Colo Notification is closed");
        ctx.stop();
    }

    fn handle(&mut self, _: ColoNotification, ctx: &mut Self::Context) {
        //self.dm_master("Master, Please prepare for SINoALICE Colo");

        for channel_id in self.config.nots.colo.iter() {
            let mut msg = message::EmbedMessage::new(COLO_TITLE, COLO_TEXT);
            msg.image = Some("https://gitlab.com/Douman/snow-white/raw/master/assets/gran_colo_banner.jpg");
            ctx.notify(message::SendMessage::embed(*channel_id, msg))
        }
    }
}

impl actix::StreamHandler<ColoGridNotification, ()> for Discord {
    fn error(&mut self, _error: (), _ctx: &mut Self::Context) -> actix::Running {
        error!("Discord: Error handling Grid Notification");
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        use actix::ActorContext;

        error!("Discord: Grid Notification is closed");
        ctx.stop();
    }

    fn handle(&mut self, _: ColoGridNotification, ctx: &mut Self::Context) {
        for channel_id in self.config.nots.colo.iter() {
            let mut msg = message::EmbedMessage::new(COLO_TITLE, COLO_TEXT_GRID);
            msg.image = Some("https://gitlab.com/Douman/snow-white/raw/master/assets/gran_colo_banner.jpg");
            ctx.notify(message::SendMessage::embed(*channel_id, msg))
        }
    }
}

impl actix::StreamHandler<DarkNotification, ()> for Discord {
    fn error(&mut self, _error: (), _ctx: &mut Self::Context) -> actix::Running {
        error!("Discord: Error handling Dark Notification");
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        use actix::ActorContext;

        error!("Discord: Dark Notification is closed");
        ctx.stop();
    }

    fn handle(&mut self, _: DarkNotification, ctx: &mut Self::Context) {
        for channel_id in self.config.nots.evo.iter() {
            let mut msg = message::EmbedMessage::new(EVO_TITLE, EVO_TEXT);
            msg.image = Some("https://gitlab.com/Douman/snow-white/raw/master/assets/Evo_Attack.png");

            ctx.notify(message::SendMessage::embed(*channel_id, msg));
        }
    }
}

struct WelcomeUser {
    user: serenity::model::guild::Member,
}

impl actix::StreamHandler<WelcomeUser, ()> for Discord {
    fn error(&mut self, _error: (), _ctx: &mut Self::Context) -> actix::Running {
        error!("Discord: Error handling Welcome");
        actix::Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        use actix::ActorContext;

        error!("Discord: Welcome Pipe is closed");
        ctx.stop();
    }

    fn handle(&mut self, msg: WelcomeUser, ctx: &mut Self::Context) {
        let user = msg.user;

        if self.config.channels.welcome > 0 {
            let mention = {
                let user = user.user.read();
                user.id.mention()
            };
            let text = format!("Everyone, please welcome {}", mention);

            ctx.notify(message::SendMessage::plain(self.config.channels.welcome, text))
        }
    }
}

impl actix::Supervised for Discord {
    fn restarting(&mut self, _: &mut actix::Context<Self>) {
        info!("Discord: Restarting");
    }
}

impl actix::Handler<message::SendMessage> for Discord {
    type Result = ();

    fn handle(&mut self, msg: message::SendMessage, ctx: &mut Self::Context) -> Self::Result {
        self.send_message(ctx, msg);
    }
}

impl actix::Handler<message::SendStats> for Discord {
    type Result = ();

    fn handle(&mut self, msg: message::SendStats, _: &mut Self::Context) -> Self::Result {
        match msg.send() {
            Ok(_) => (),
            Err(error) => warn!("Discord: Cannot send stats: {}", error),
        }
    }
}

impl actix::Handler<message::StopActor> for Discord {
    type Result = ();

    fn handle(&mut self, _msg: message::StopActor, ctx: &mut Self::Context) -> Self::Result {
        use actix::ActorContext;
        warn!("Discord: Shutdown command is issued!");
        STATS.increment(stats::ActorShutdown);

        ctx.stop();
    }
}
