use strsim::normalized_damerau_levenshtein as similarity;
//use strsim::jaro_winkler as similarity;

const THANK_REPLY: &'static str = "You're welcome";
const SNOW_CUTE_REPLY: &'static str = "I don't need your flattery";

pub fn reply_to(msg: &str) -> Option<&str> {
    const MIN_SIMILAR: f64 = 0.7;

    let msg = msg.trim();

    let msg = msg.to_lowercase();
    if !msg.contains("snow") {
        return None;
    }
    let msg = msg.as_str();

    if similarity(msg, "thank you snow") > MIN_SIMILAR {
        Some(THANK_REPLY)
    } else if similarity(msg, "snow cute") > MIN_SIMILAR {
        Some(SNOW_CUTE_REPLY)
    } else if similarity(msg, "you cute snow") > MIN_SIMILAR {
        Some(SNOW_CUTE_REPLY)
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cute() {
        let result = reply_to("Snow cute");
        assert!(result.is_some());
        assert_eq!(SNOW_CUTE_REPLY, result.unwrap());

        let result = reply_to("Snow is cute");
        assert!(result.is_some());
        assert_eq!(SNOW_CUTE_REPLY, result.unwrap());

        let result = reply_to("You cute snow");
        assert!(result.is_some());
        assert_eq!(SNOW_CUTE_REPLY, result.unwrap());

        let result = reply_to("You are cute snow");
        assert!(result.is_some());
        assert_eq!(SNOW_CUTE_REPLY, result.unwrap());
    }

    #[test]
    fn test_thanks() {
        let result = reply_to("Thank you Snow");
        assert!(result.is_some());
        assert_eq!(THANK_REPLY, result.unwrap());

        let result = reply_to("Thanks Snow");
        assert!(result.is_some());
        assert_eq!(THANK_REPLY, result.unwrap());

        let result = reply_to("Thank you");
        assert!(result.is_none());
    }
}

