use std::time;

pub const JST_OFFSET: i32 = 9 * 3600; //9h

pub const CONFIG_NAME: &'static str = "discord.toml";

pub const CMD_PREFIX: &'static str = ".";
pub const TOKEN: &'static str = include_str!("system/DISCORD_TOKEN");
pub const DAY_DURATION: time::Duration = time::Duration::from_secs(86400);
pub const UPDATE_CONFIG_PERIOD: time::Duration = time::Duration::from_secs(60); //1min

//Utilities
pub const GUILD_ONLY_CMD: &'static str = "I'm sorry, but this command is allowed only on servers, and not DMs";
pub const WELCOME_SET: &'static str = "Welcome channel has been set";

//Colo
pub const COLO_TITLE: &'static str = "Colo Time";
pub const COLO_TEXT: &'static str = "Prepare for battle

It is 15 minute until Colo.
Polish your sword and spirit.
Get ready to crush these sinful souls!

**Be sure to send sticker when you enter battlefield!**
Don't make Maru-san sad.

";

pub const COLO_TEXT_GRID: &'static str = "Prepare your equipment

It is 45 minute until Colo.
You have only 15 minutes to prepare your grid!
Do not neglect, or you shall be punished for your carelessness!

";

//Evo
pub const EVO_TITLE: &'static str = "Evo Grind";
pub const EVO_TEXT: &'static str = "Heads up!

It is 15 minute until Evo event is going to happen.
Prepare to grind it!
";

//Library command text
pub const LIBRARY_TITLE: &'static str = "Library";
pub const LIBRARY_TEXT: &'static str = "

__**Links:**__

[Database](https://sinoalice.game-db.tw/)
[Translations](http://sinoalice.moe/directory)
[SINoALICE Twatter](https://twitter.com/sinoalice_jp)

";

//Help related text
pub const HELP_TITLE: &'static str = "Help";
pub const HELP_TEXT: &'static str = "__**Commands:**__

**roll** - Rolls some dnd dice.
**library** - Lists useful information.
**judge** - Judge one among provided options.
**subscribe** - Performs subscribe of channel to notifications.
**unsubscribe** - Unsubscribe channel from notification.
**time** - Tells you current time.
**set_colo_time** - Sets new time for Colo notifications.
**set_welcome** - Sets welcoming channel.

To get help with an individual command, pass its name as an argument to this command.
";
pub const HELP_SUBSCRIBE: &'static str = "**Usage:** subscribe <type>

Subscribes to notification in current channel.

Available types:
__colo__ - Perform subscription on colo notifications.
__evo__ - Perform subscription on evo daily notifications.
";
pub const HELP_UNSUB: &'static str = "**Usage:** unsubscribe <type>

Unsubscribe channel from notification.

Available types:
__colo__ - Remove subscription on colo notifications.
__evo__ - Remove subscription on evo daily notifications.
";
pub const HELP_LIBRARY: &'static str = "**Usage:** library

Lists useful information.
";
pub const HELP_JUDGE: &'static str = "**Usage:** judge [first][ second]...[ N]

Randomly choose one among provided options separated by white space
";
pub const HELP_TIME: &'static str = "**Usage:** time

Tells you current time.
Snow provides time in UTC and JST time zones.
";
pub const HELP_SET_COLO_TIME: &'static str = "**Usage:** set_colo_time <JST time>

Changes time of Colo notifications
Time must be provided in following format: `HH:MM:SS`
";
pub const HELP_ROLL: &'static str = "**Usage:** roll <dice>

Rolls dnd dice such as 2d20+10
";
pub const HELP_SET_WELCOME: &'static str = "**Usage:** set_welcome

Sets channel as welcoming one.
The channel shall be used to greet newcomers.
";
