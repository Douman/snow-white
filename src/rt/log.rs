extern crate cute_log;
extern crate lazy_panic;

#[cfg(not(debug_assertions))]
type PanicHandler = lazy_panic::formatter::Simple;

#[cfg(debug_assertions)]
type PanicHandler = lazy_panic::formatter::Debug;

/// Initializes logging facilities
pub fn init() {
    set_panic_message!(PanicHandler);
    cute_log::init().expect("To initialize log");
}
